/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication_1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author portatil
 */
public class FizzBuzzJUnitTest {

    static FizzBuzzOthers mainFBO;

    public FizzBuzzJUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        mainFBO = new FizzBuzzOthers();
    }

    @After
    public void tearDown() {
    }

// TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testFizzNum6() {
        String resEsperat = "FIZZ";
        String resReal = mainFBO.fizzBuzz(6);
        assertEquals(resEsperat, resReal);
    }

    @Test
    public void testFizzNum4() {
        String resEsperat = "";
        String resReal = mainFBO.fizzBuzz(4);
        assertEquals(resEsperat, resReal);
    }

    @Test
    public void testFizzNum10() {
        String resEsperat = "BUZZ";
        String resReal = mainFBO.fizzBuzz(10);
        assertEquals(resEsperat, resReal);
    }

    @Test
    public void testFizzNum15() {
        String resEsperat = "FIZZ BUZZ";
        String resReal = mainFBO.fizzBuzz(15);
        assertEquals(resEsperat, resReal);
    }

    @Test
    public void testFizzNum16() {
        String resEsperat = ""; //LO QUE YO ESPERO
        String resReal = mainFBO.fizzBuzz(16);
        assertEquals(resEsperat, resReal);
    }

    @Test
    public void testFizzNum25() {
        String resEsperat = "BUZZ";
        String resReal = mainFBO.fizzBuzz(25);
        assertEquals(resEsperat, resReal);
    }

    @Test
    public void testFizzNum125() {
        String resEsperat = "BUZZ";
        String resReal = mainFBO.fizzBuzz(125);
        assertEquals(resEsperat, resReal);
    }

    
    
    @Test
    public void testFizzNum30() {
        String resEsperat = "FIZZ BUZZ";
        String resReal = mainFBO.fizzBuzz(30);
        assertEquals(resEsperat, resReal);
    }
    
    @Test
    public void testFizzNum50() {
        String resEsperat = "BUZZ";
        String resReal = mainFBO.fizzBuzz(50);
        assertEquals(resEsperat, resReal);
    }
    
}
