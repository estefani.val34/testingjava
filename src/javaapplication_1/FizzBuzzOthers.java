/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication_1;

/**
 *
 * @author Alumne
 */
public class FizzBuzzOthers {

    private boolean isMultiple(int num, int multiple) {
        return num%multiple==0;
    }
	
    public String fizzBuzz(int number) {
    
        if (isMultiple(number,15)) {
            return "FIZZ BUZZ";
        }
        if (isMultiple(number,3)) {
            return "FIZZ";
        }
        if (isMultiple(number,5)) {
            return "BUZZ";
        }
        return "";
    }
    
   
   
    
    
    
}
