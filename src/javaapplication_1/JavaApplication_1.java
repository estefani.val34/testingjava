/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author portatil
 */
public class JavaApplication_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String name, message;
        System.out.println("De quin curs ets?");
        name=br.readLine();
        message=classificacio(name);
        System.out.println(message);
        
        FizzBuzzOthers fizz=new FizzBuzzOthers ();
        System.out.println(fizz.fizzBuzz(3));
       
    }
    
    
    public static String classificacio(String name){
        String classification;
        switch(name){
            case "de primer":
                classification="Ohhhh encara et falta un curs!!!";
                break;
            case "de segon":
                classification="Ja no et falta res!!!anims!!!";
                break;
            default:
                classification="Aqueta opció no és correcta";
        }
        return classification;
        
    }
}
