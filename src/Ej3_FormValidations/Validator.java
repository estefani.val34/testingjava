package Ej3_FormValidations;

import java.util.*;

public class Validator {

    /**
     * This function checks if the field is empty
     *
     * @param field
     * @return boolean
     */
    public boolean valNotEmpty(String field) {
        return field != null && !field.isEmpty();
    }

    /**
     * This function checks if the first letter of the field is uppercase
     *
     * @param field
     * @return boolean
     */
    public boolean valFirstLetterUppercase(String field) {
        String regex = "^[A-Z][a-z]+$";
        return field.matches(regex);
    }

    /**
     * This function checks if the field is shorter than or equal to the max
     * length
     *
     * @param field
     * @param maxLength
     * @return boolean
     */
    public boolean valMaxLength(String field, int maxLength) {
        return field.length() <= maxLength;
    }

    /**
     * This function checks if the field email is valid
     *
     * @param email
     * @return boolean
     */
    public boolean valEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    /**
     * This function checks if the field URL is valid
     *
     * @param URL
     * @return boolean
     */
    public boolean valURL(String URL) {
        String regex = "(http://||https://)?(www\\.)?[a-zA-Z0-9]+(\\.[a-zA-Z0-9])*(\\.[a-z]{3})+/?";
        return URL.matches(regex);
    }

    /**
     * This functions checks if the field contains forbidden words
     *
     * @param field
     * @return boolean
     */
    public boolean valNotForbiddenWords(String field) {
        ArrayList<String> forbiddenWords = new ArrayList<>();
        String[] words = {"ximple", "imbècil", "babau", "inútil", "burro", "loser", "noob", "capsigrany", "torrecollons", "fatxa", "nazi", "supremacista"};
        Collections.addAll(forbiddenWords, words);
        return forbiddenWords.contains(field);
    }

     /**
     * This functions check  genere : masculi, femeni, altres
     * @param gen
     * @return  bool
     */

    public boolean valgenere(String gen) {
        ArrayList<String> words = new ArrayList<>();
        words.add("masculi");
        words.add("femeni");
        words.add("altres");
        return words.contains(gen);
    }
    
    /**
     * verifica la  situacio laboral equival a : estudiant, desempleat,treballador, autonom . Per listas
     * @param gen
     * @return  bool
     */

    public boolean vallistavalorreduida(String situacio){
        ArrayList<String> words = new ArrayList<>();
        words.add("estudiant");
        words.add("desempleat");
        words.add("treballador");
        return  words.contains(situacio);
    }
    
    
}
